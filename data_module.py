"""Управление данными о URLs"""
import pickle


##To save in file
def save_data(data: set) -> None:
    """Сохраняет словарь в файл"""
    with open("data.txt", "wb") as file:
        pickle.dump(data, file)


def load_data() -> set:
    """Возвращает словарь с URLs из файла"""
    with open("data.txt", "rb") as file:
        return pickle.load(file)
