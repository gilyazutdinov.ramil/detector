"""Управление переодическими заданиями"""
import logging
from telegram.ext import CallbackContext
from check_urls import step
from consts import CHECKINTERVAL


def _check_websites(context: CallbackContext) -> None:
    """Единаразово запускает задание на проверку сайтов и отправляет сообщение о его результатах"""
    job = context.job
    if job:
        message = str(step())
        context.bot.send_message(job.context, text=message)
    else:
        logging.error("Не найдено задание на проверк")


def start_new_job(name: str, context: CallbackContext) -> None:
    """Запускает переодическое задание на проверку сайтов"""
    context.job_queue.run_repeating(  # type: ignore
        _check_websites, context=name, name=name, interval=CHECKINTERVAL
    )


def remove_job_by_name(name: str, context: CallbackContext) -> None:
    """Удаляет задание по имени, если оно есть"""
    current_jobs = context.job_queue.get_jobs_by_name(name)  # type: ignore
    for job in current_jobs:
        job.schedule_removal()
