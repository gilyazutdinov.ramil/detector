"""Файл констант"""
from config import config


TELEBOTID = config["TELEBOTID"]
GOOGLE_API_KEY = config["GOOGLE_API_KEY"]
CHECKINTERVAL = 60 * 60  # one hour - как часто проверять
BAD_RESPONSE_TIME = 2.0  # sec - какое время отклика страницы считать "плохим"
