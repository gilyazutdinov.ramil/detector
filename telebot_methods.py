"""Все действия TG бота"""
from pathlib import Path
from telegram import Update
from telegram.ext import CallbackContext
from tasks import remove_job_by_name, start_new_job
from data_module import save_data, load_data
from check_urls import step


def start(update: Update, context: CallbackContext) -> None:
    """Запускает переодическое задание на проверку URLs"""
    job_name = str(update.effective_chat.id)  # type: ignore
    remove_job_by_name(job_name, context)
    start_new_job(job_name, context)

    message = "Задание на проверку URLs успешно запущено"
    update.message.reply_text(message)


def stop(update: Update, context: CallbackContext) -> None:
    """Останавливает переодическое задание на проверку URLs"""
    chat_id = update.message.chat_id
    remove_job_by_name(str(chat_id), context)
    message = "Задание на проверку URLs успешно остановлено"
    update.message.reply_text(message)


def delete(update: Update, context: CallbackContext) -> None:
    """Удаляет URL из списки на проверку"""
    if context.args and context.args[0]:
        url = context.args[0]
        _data = load_data()
        if url in _data:
            _data.remove(url)
        save_data(_data)
        context.bot.send_message(chat_id=update.message.chat_id, text="Url удален")
    else:
        context.bot.send_message(
            chat_id=update.message.chat_id,
            text="URL НЕ принят, укажите URL после команды",
        )


def clear(update: Update, context: CallbackContext) -> None:
    """Удаляет все URLЫ из списки на проверку"""
    _data: set = set()
    save_data(_data)
    context.bot.send_message(chat_id=update.message.chat_id, text="Список URLs очищен")


def add_urls(update: Update, context: CallbackContext) -> None:
    """Добавляет URL в список для проверки"""
    if context.args and context.args[0]:
        url = context.args[0]
        _data = load_data()
        _data.update({url})
        save_data(_data)
        context.bot.send_message(chat_id=update.message.chat_id, text="URL принят")
    else:
        context.bot.send_message(
            chat_id=update.message.chat_id,
            text="URL НЕ принят, укажите URL после команды",
        )


def get_urls(update: Update, context: CallbackContext) -> None:
    """Отправляет список URLs для проверки"""
    context.bot.send_message(chat_id=update.message.chat_id, text=str(load_data()))


def about_bot(update: Update, context: CallbackContext) -> None:
    """Отправляет Help о боте"""
    context.bot.send_message(
        chat_id=update.message.chat_id, text=Path("help.md").read_text(encoding="utf-8")
    )


def check(update: Update, context: CallbackContext) -> None:
    """Немедленно запускает задание и отправляет сообщение о его результатах"""
    context.bot.send_message(
        chat_id=update.message.chat_id, text="Проверка запущена, ожидайте ответа"
    )
    context.bot.send_message(chat_id=update.message.chat_id, text=str(step()))
