"""TG бот для мониторинга статуса сайта/ов"""
from telegram.ext import Updater, CommandHandler
from telebot_methods import (
    start,
    stop,
    get_urls,
    add_urls,
    delete,
    clear,
    check,
    about_bot,
)

from consts import TELEBOTID

updater = Updater(token=TELEBOTID, use_context=True)
dispatcher = updater.dispatcher  # type: ignore

dispatcher.add_handler(CommandHandler("start", start))
dispatcher.add_handler(CommandHandler("stop", stop))
dispatcher.add_handler(CommandHandler("list", get_urls))
dispatcher.add_handler(CommandHandler("add", add_urls))
dispatcher.add_handler(CommandHandler("delete", delete))
dispatcher.add_handler(CommandHandler("clear", clear))
dispatcher.add_handler(CommandHandler("check", check))
dispatcher.add_handler(CommandHandler("help", about_bot))

updater.start_polling()
updater.idle()
