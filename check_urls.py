"""Реализация одного шага переодической проверки URLs."""
import urllib.request
import json
from data_module import load_data
from consts import BAD_RESPONSE_TIME, GOOGLE_API_KEY


def step() -> dict[str, list]:
    """Делает одну проверку на время "отклика" по всем URLs и формирует ответ-статус"""
    result: dict[str, list] = {}
    is_fine: bool = True
    for url in load_data():
        try:
            req = urllib.request.Request(
                "https://www.googleapis.com/pagespeedonline/v5/runPagespeed?"
                "url=" + url + "&key=" + GOOGLE_API_KEY
                if GOOGLE_API_KEY
                else "https://www.googleapis.com/pagespeedonline/v5/runPagespeed?"
                "url=" + url
            )
            with urllib.request.urlopen(req) as response:
                data = json.loads(response.read())
                overall_score: float = data["lighthouseResult"]["categories"][
                    "performance"
                ]["score"]
                if overall_score > BAD_RESPONSE_TIME:
                    is_fine = False
                result.setdefault("URL", []).append(url)
                result.setdefault("Скорость", []).append(overall_score)
        except:  # pylint: disable=bare-except
            is_fine = False
            result.setdefault("URL", []).append(url)
            result.setdefault("Скорость", []).append(
                "Ошибка сканирования, проверьте URL"
            )
            break
    result.setdefault("is_fine", []).append(is_fine)
    return result
